<?php
defined('iPHP') OR exit('Access Denied');
return array (
  'admincp' => 
  array (
    'sidebar' => 
    array (
      'enable' => '1',
      'mini' => '0',
      'right' => '0',
    ),
  ),
  'api' => 
  array (
    'baidu' => 
    array (
      'sitemap' => 
      array (
        'site' => '',
        'access_token' => '',
        'sync' => '0',
      ),
    ),
  ),
  'apps' => 
  array (
    'article' => 1,
    'node' => 2,
    'tag' => 3,
    'category' => 4,
    'comment' => 5,
    'prop' => 6,
    'message' => 7,
    'favorite' => 8,
    'user' => 9,
    'admincp' => 10,
    'config' => 11,
    'files' => 12,
    'menu' => 13,
    'role' => 14,
    'member' => 15,
    'editor' => 16,
    'apps' => 17,
    'former' => 18,
    'patch' => 19,
    'content' => 20,
    'index' => 21,
    'public' => 22,
    'cache' => 23,
    'filter' => 24,
    'plugin' => 25,
    'forms' => 26,
    'vote' => 27,
    'chain' => 28,
    'links' => 29,
    'search' => 31,
    'database' => 32,
    'html' => 33,
    'spider' => 34,
    'archive' => 35,
    'developer' => 36,
    'payment' => 37,
    'forum' => 43,
  ),
  'cache' => 
  array (
    'engine' => 'file',
    'host' => '',
    'time' => '300',
    'compress' => '0',
    'page_total' => '300',
    'prefix' => 'iCMS',
  ),
  'CDN' => 
  array (
    'enable' => '0',
    'cache_control' => 'public',
    'expires' => '86400',
  ),
  'debug' => 
  array (
    'php' => '1',
    'php_trace' => '0',
    'php_errorlog' => '0',
    'access_log' => '0',
    'tpl' => '1',
    'tpl_trace' => '0',
    'db' => '1',
    'db_trace' => '0',
    'db_explain' => '0',
    'db_optimize_in' => '0',
  ),
  'FS' => 
  array (
    'url' => 'http://v8.icmsdev.com/res/',
    'dir' => 'res',
    'dir_format' => '{md5:0,2}/{md5:2,3}/',
    'allow_ext' => 'gif,jpg,rar,swf,jpeg,png,zip,mp4',
    'check_md5' => '0',
  ),
  'mail' => 
  array (
    'host' => '',
    'secure' => '',
    'port' => '25',
    'username' => '',
    'password' => '',
    'setfrom' => '',
    'replyto' => '',
  ),
  'member' => 
  array (
    'life' => '86400000',
  ),
  'plugin' => 
  array (
    'SMS' => 
    array (
      'aliyun' => 
      array (
        'AccessKeyId' => '',
        'AccessKeySecret' => '',
        'TemplateCode' => '',
        'SignName' => '',
      ),
      'expire' => '60',
    ),
  ),
  'publish' => 
  array (
  ),
  'route' => 
  array (
    'url' => 'http://v8.icmsdev.com',
    'redirect' => '0',
    404 => 'http://v8.icmsdev.com/public/404.htm',
    'public' => 'http://v8.icmsdev.com/public',
    'user' => 'http://v8.icmsdev.com/user',
    'dir' => '/',
    'ext' => '.html',
    'speed' => '5',
    'rewrite' => '0',
  ),
  'site' => 
  array (
    'name' => 'iCMS',
    'seotitle' => '给我一套程序，我能搅动互联网',
    'keywords' => 'iCMS,iCMS内容管理系统,文章管理系统,PHP文章管理系统',
    'description' => 'iCMS 是一套采用 PHP 和 MySQL 构建的高效简洁的内容管理系统,为您的网站提供一个完美的开源解决方案',
    'code' => '',
    'icp' => '',
  ),
  'sphinx' => 
  array (
    'host' => '127.0.0.1:9312',
    'index' => 
    array (
      'article' => 'iCMS_article iCMS_article_delta',
    ),
  ),
  'system' => 
  array (
    'patch' => '1',
  ),
  'taoke' => 
  array (
    'pid' => '',
  ),
  'template' => 
  array (
    'index' => 
    array (
      'mode' => '0',
      'rewrite' => '1',
      'tpl' => '{iTPL}/index.htm',
      'name' => 'index',
    ),
    'desktop' => 
    array (
      'domain' => 'http://v8.icmsdev.com',
      'name' => 'desktop',
      'agent' => '',
      'tpl' => 'blog',
      'index' => '{iTPL}/index.htm',
    ),
    'mobile' => 
    array (
      'name' => 'mobile',
      'agent' => 'WAP,Smartphone,Mobile,UCWEB,Opera Mini,Windows CE,Symbian,SAMSUNG,iPhone,Android,BlackBerry,HTC,Mini,LG,SonyEricsson,J2ME,MOT',
      'domain' => 'http://m.v8.icmsdev.com',
      'tpl' => 'blog',
      'index' => '{iTPL}/index.htm',
    ),
  ),
  'thumb' => 
  array (
    'size' => '',
  ),
  'time' => 
  array (
    'zone' => 'Asia/Shanghai',
    'cvtime' => '0',
    'dateformat' => 'Y-m-d H:i:s',
  ),
  'watermark' => 
  array (
    'enable' => '0',
    'mode' => '0',
    'pos' => '8',
    'x' => '10',
    'y' => '10',
    'width' => '140',
    'height' => '140',
    'allow_ext' => 'jpg,jpeg,png',
    'img' => 'watermark.png',
    'transparent' => '80',
    'text' => 'iCMS',
    'font' => '',
    'fontsize' => '24',
    'color' => '#000000',
    'mosaics' => 
    array (
      'width' => '150',
      'height' => '90',
      'deep' => '9',
    ),
  ),
  'article' => 
  array (
    'img_title' => '0',
    'pic_center' => '1',
    'pic_next' => '0',
    'pageno_incr' => '',
    'markdown' => '0',
    'autoformat' => '0',
    'catch_remote' => '1',
    'remote' => '0',
    'autopic' => '0',
    'autodesc' => '1',
    'descLen' => '100',
    'autoPage' => '0',
    'AutoPageLen' => '',
    'repeatitle' => '0',
    'showpic' => '0',
    'filter' => 
    array (
      0 => 'description:简介',
      1 => 'body:内容',
      2 => 'stitle:短标题',
      3 => 'keywords:关键字',
    ),
    'totalNum' => '',
    'clink' => '-',
    'emoji' => '',
    'sphinx' => 
    array (
      'host' => '',
      'index' => '',
    ),
  ),
  'node' => 
  array (
    'domain' => 
    array (
      'http://v8test.icmsdev.com' => 4,
    ),
  ),
  'tag' => 
  array (
    'rule' => '{PHP}',
    'tpl' => '{iTPL}/tag.htm',
    'tkey' => '-',
  ),
  'comment' => 
  array (
    'enable' => '1',
    'examine' => '1',
    'captcha' => '1',
    'reply' => 
    array (
      'enable' => '1',
      'examine' => '0',
      'captcha' => '0',
    ),
  ),
  'user' => 
  array (
    'register' => 
    array (
      'enable' => '1',
      'mode' => 
      array (
        0 => 'account',
      ),
      'modeText' => 
      array (
        'account' => '用户名',
        'phone' => '手机号',
        'email' => '邮箱',
      ),
      'verify' => 
      array (
        'phone' => '0',
        'email' => '0',
      ),
      'captcha' => '0',
      'interval' => '600',
      'role' => '3',
    ),
    'agreement' => '',
    'login' => 
    array (
      'enable' => '1',
      'modeText' => 
      array (
        'phone' => '免密登录',
        'account' => '密码登录',
        'weixin' => '扫码登录',
      ),
      'mode' => 
      array (
        0 => 'account',
      ),
      'auto_register' => '0',
      'captcha' => '0',
      'interval' => '10',
      'times' => '2',
    ),
    'post' => 
    array (
      'captcha' => '1',
      'interval' => '10',
    ),
    'forward' => '0',
    'coverpic' => '/ui/coverpic.jpg',
    'node' => 
    array (
      'max' => '10',
    ),
    'report' => 
    array (
      'reason' => '垃圾广告信息
不实信息
辱骂、人身攻击等不友善行为
有害信息
涉嫌侵权
诱导赞同、关注等行为',
    ),
    'open' => 
    array (
      'WX' => 
      array (
        'enable' => '0',
        'id' => '1',
        'name' => '微信',
        'appid' => '',
        'appkey' => '',
        'redirect' => '',
      ),
      'QQ' => 
      array (
        'enable' => '0',
        'id' => '2',
        'name' => 'QQ',
        'appid' => '',
        'appkey' => '',
        'redirect' => '',
      ),
      'WB' => 
      array (
        'enable' => '0',
        'id' => '3',
        'name' => '微博',
        'appid' => '',
        'appkey' => '',
        'redirect' => '',
      ),
      'TB' => 
      array (
        'enable' => '0',
        'id' => '4',
        'name' => '淘宝',
        'appid' => '',
        'appkey' => '',
        'redirect' => '',
      ),
    ),
  ),
  'cloud' => 
  array (
    'enable' => '0',
    'local' => '0',
    'vendor' => 
    array (
      'AliYunOSS' => 
      array (
        'BucketDomain' => '',
        'Bucket' => '',
        'AccessKey' => '',
        'SecretKey' => '',
        'Dir' => '',
        'domain' => '',
      ),
    ),
  ),
  'files' => 
  array (
  ),
  'APPS:META' => 
  array (
    'actor' => 1,
    'apps' => 1,
    'article' => 1,
    'config' => 1,
    'forms' => 1,
    'forum' => 1,
    'links' => 1,
    'meta' => 1,
    'node' => 1,
    'tag' => 1,
    'test' => 1,
    'type2' => 1,
    'type4' => 1,
    'user' => 1,
  ),
  'hooks' => 
  array (
    'fields' => 
    array (
      'article' => 
      array (
        'body' => 
        array (
          0 => 'LinksApp::HOOK_run',
          1 => 'PluginTaoke::HOOK',
        ),
      ),
    ),
  ),
  'chain' => 
  array (
    'limit' => '-1',
  ),
  'links' => 
  array (
    'base' => 'http://v8.icmsdev.com/public/api.php?app=links',
    'template' => '/tools/links.target.htm',
  ),
  'payment' => 
  array (
    'enable' => '0',
    'anonymous' => '0',
    'cookie' => '0',
    'name' => 'i币',
    'unit' => '个',
    'expire' => '600',
    'wx' => 
    array (
      'use_sandbox' => '0',
      'interface' => '',
      'app_id' => '',
      'mch_id' => '',
      'mch_key' => '',
      'notify_url' => '',
      'redirect_url' => '',
      'sslcert' => '',
      'sslkey' => '',
      'return_raw' => '0',
    ),
    'ali' => 
    array (
      'use_sandbox' => '0',
      'interface' => '',
      'app_id' => '',
      'public_key' => '',
      'private_key' => '',
      'notify_url' => '',
      'redirect_url' => '',
      'return_raw' => '0',
    ),
    'charge' => 
    array (
      'ratio' => '1',
      'max' => '50000',
      'info' => '',
    ),
  ),
  'forum' => 
  array (
    'tag' => '已解决,未解决',
    'classify' => '热门标签1,热门标签2',
  ),
  'iurl' => 
  array (
    'article' => 
    array (
      'type' => '2',
      'primary' => 'id',
      'page' => 'p',
      'sort' => 3,
    ),
    'node' => 
    array (
      'type' => '1',
      'primary' => 'id',
      'sort' => 2,
    ),
    'tag' => 
    array (
      'type' => '3',
      'primary' => 'id',
      'sort' => 4,
    ),
    'category' => 
    array (
      'rule' => '1',
      'primary' => 'cid',
    ),
    'content' => 
    array (
      'type' => '4',
      'primary' => 'id',
      'page' => 'p',
      'sort' => 5,
    ),
    'index' => 
    array (
      'type' => '0',
      'primary' => '',
      'sort' => 1,
    ),
    'forum' => 
    array (
      'rule' => '2',
      'primary' => 'id',
      'page' => 'p',
    ),
  ),
  'routing' => 
  array (
    'ArticleUser' => 
    array (
      0 => '/ArticleUser',
      1 => 'api.php?app=ArticleUser',
      'sort' => 64,
    ),
    'ArticleUser:manage' => 
    array (
      0 => '/ArticleUser',
      1 => 'api.php?app=ArticleUser&do=manage',
      'sort' => 66,
    ),
    'ArticleUser:publish' => 
    array (
      0 => '/ArticleUser/publish',
      1 => 'api.php?app=ArticleUser&do=publish',
      'sort' => 65,
    ),
    'CommentUser:manage' => 
    array (
      0 => '/CommentUser/manage',
      1 => 'api.php?app=CommentUser&do=manage',
      'sort' => 52,
    ),
    'FilesUser' => 
    array (
      0 => '/FilesUser',
      1 => 'api.php?app=FilesUser',
      'sort' => 55,
    ),
    'FilesUser:add' => 
    array (
      0 => '/FilesUser/add',
      1 => 'api.php?app=FilesUser&do=add',
      'sort' => 56,
    ),
    'FilesUser:browse' => 
    array (
      0 => '/FilesUser/browse',
      1 => 'api.php?app=FilesUser&do=browse',
      'sort' => 58,
    ),
    'FilesUser:multi' => 
    array (
      0 => '/FilesUser/multi',
      1 => 'api.php?app=FilesUser&do=multi',
      'sort' => 57,
    ),
    'FilesUser:preview' => 
    array (
      0 => '/FilesUser/preview',
      1 => 'api.php?app=FilesUser&do=preview',
      'sort' => 59,
    ),
    'ForumUser' => 
    array (
      0 => '/ForumUser',
      1 => 'api.php?app=ForumUser',
      'sort' => 61,
    ),
    'ForumUser:manage' => 
    array (
      0 => '/ForumUser',
      1 => 'api.php?app=ForumUser&do=manage',
      'sort' => 63,
    ),
    'ForumUser:publish' => 
    array (
      0 => '/ForumUser/publish',
      1 => 'api.php?app=ForumUser&do=publish',
      'sort' => 62,
    ),
    'UserNode' => 
    array (
      0 => '/UserNode',
      1 => 'api.php?app=UserNode',
      'sort' => 42,
    ),
    'UserNode:manage' => 
    array (
      0 => '/UserNode/manage',
      1 => 'api.php?app=UserNode&do=manage',
      'sort' => 41,
    ),
    'UserProfile' => 
    array (
      0 => '/UserProfile',
      1 => 'api.php?app=UserProfile',
      'sort' => 40,
    ),
    'api' => 
    array (
      0 => '/api',
      1 => 'api.php',
      'sort' => 60,
    ),
    'comment' => 
    array (
      0 => '/comment',
      1 => 'api.php?app=comment',
      'sort' => 51,
    ),
    'favorite' => 
    array (
      0 => '/favorite',
      1 => 'api.php?app=favorite',
      'sort' => 53,
    ),
    'favorite:id' => 
    array (
      0 => '/favorite/{id}/',
      1 => 'api.php?app=favorite&id={id}',
      'sort' => 54,
    ),
    'forms' => 
    array (
      0 => '/forms',
      1 => 'api.php?app=forms',
      'sort' => 43,
    ),
    'forms:id' => 
    array (
      0 => '/forms/{id}/',
      1 => 'api.php?app=forms&id={id}',
      'sort' => 45,
    ),
    'forms:save' => 
    array (
      0 => '/forms/save',
      1 => 'api.php?app=forms&action=save',
      'sort' => 44,
    ),
    'public:captcha' => 
    array (
      0 => '/public/captcha',
      1 => 'api.php?app=public&do=captcha',
      'sort' => 46,
    ),
    'public:license:charge' => 
    array (
      0 => '/public/license/charge',
      1 => 'api.php?app=public&do=license&s=charge',
      'sort' => 49,
    ),
    'public:privacy' => 
    array (
      0 => '/public/privacy',
      1 => 'api.php?app=public&do=privacy',
      'sort' => 47,
    ),
    'public:terms' => 
    array (
      0 => '/public/terms',
      1 => 'api.php?app=public&do=terms',
      'sort' => 48,
    ),
    'search' => 
    array (
      0 => '/search',
      1 => 'api.php?app=search',
      'sort' => 50,
    ),
    'uid:cid' => 
    array (
      0 => '/{uid}/{cid}/',
      1 => 'api.php?app=user&do=home&uid={uid}&cid={cid}',
      'sort' => 29,
    ),
    'uid:comment' => 
    array (
      0 => '/{uid}/comment/',
      1 => 'api.php?app=user&do=comment&uid={uid}',
      'sort' => 24,
    ),
    'uid:fans' => 
    array (
      0 => '/{uid}/fans/',
      1 => 'api.php?app=user&do=fans&uid={uid}',
      'sort' => 27,
    ),
    'uid:favorite' => 
    array (
      0 => '/{uid}/favorite/',
      1 => 'api.php?app=user&do=favorite&uid={uid}',
      'sort' => 26,
    ),
    'uid:favorite:id' => 
    array (
      0 => '/{uid}/favorite/{id}/',
      1 => 'api.php?app=user&do=favorite&uid={uid}&id={id}',
      'sort' => 30,
    ),
    'uid:follower' => 
    array (
      0 => '/{uid}/follower/',
      1 => 'api.php?app=user&do=follower&uid={uid}',
      'sort' => 28,
    ),
    'uid:home' => 
    array (
      0 => '/{uid}/',
      1 => 'api.php?app=user&do=home&uid={uid}',
      'sort' => 23,
    ),
    'uid:share' => 
    array (
      0 => '/{uid}/share/',
      1 => 'api.php?app=user&do=share&uid={uid}',
      'sort' => 25,
    ),
    'user' => 
    array (
      0 => '/user',
      1 => 'api.php?app=user',
      'sort' => 0,
    ),
    'user:article' => 
    array (
      0 => '/user/article',
      1 => 'api.php?app=user&do=manage&pg=article',
      'sort' => 3,
    ),
    'user:content:home' => 
    array (
      0 => '/user/content/home',
      1 => 'api.php?app=user&do=content&s=home',
      'sort' => 31,
    ),
    'user:findpwd' => 
    array (
      0 => '/user/findpwd',
      1 => 'api.php?app=user&do=findpwd',
      'sort' => 22,
    ),
    'user:home' => 
    array (
      0 => '/user/home',
      1 => 'api.php?app=user&do=home',
      'sort' => 1,
    ),
    'user:login' => 
    array (
      0 => '/user/login',
      1 => 'api.php?app=user&do=login',
      'sort' => 18,
    ),
    'user:login:qq' => 
    array (
      0 => '/user/login/qq',
      1 => 'api.php?app=user&do=login&sign=qq',
      'sort' => 19,
    ),
    'user:login:qrcode' => 
    array (
      0 => '/user/login/qrcode',
      1 => 'api.php?app=user&do=login&s=qrcode',
      'sort' => 33,
    ),
    'user:login:wb' => 
    array (
      0 => '/user/login/wb',
      1 => 'api.php?app=user&do=login&sign=wb',
      'sort' => 20,
    ),
    'user:login:wx' => 
    array (
      0 => '/user/login/wx',
      1 => 'api.php?app=user&do=login&sign=wx',
      'sort' => 21,
    ),
    'user:logout' => 
    array (
      0 => '/user/logout',
      1 => 'api.php?app=user&do=logout',
      'sort' => 17,
    ),
    'user:manage' => 
    array (
      0 => '/user/manage',
      1 => 'api.php?app=user&do=manage',
      'sort' => 4,
    ),
    'user:manage:category' => 
    array (
      0 => '/user/manage/category',
      1 => 'api.php?app=user&do=manage&s=category',
      'sort' => 5,
    ),
    'user:manage:charge' => 
    array (
      0 => '/user/manage/charge',
      1 => 'api.php?app=user&do=manage&s=charge',
      'sort' => 39,
    ),
    'user:manage:comment' => 
    array (
      0 => '/user/manage/comment',
      1 => 'api.php?app=user&do=manage&s=comment',
      'sort' => 6,
    ),
    'user:manage:email' => 
    array (
      0 => '/user/manage/email',
      1 => 'api.php?app=user&do=manage&s=email',
      'sort' => 37,
    ),
    'user:manage:fans' => 
    array (
      0 => '/user/manage/fans',
      1 => 'api.php?app=user&do=manage&s=fans',
      'sort' => 8,
    ),
    'user:manage:favorite' => 
    array (
      0 => '/user/manage/favorite',
      1 => 'api.php?app=user&do=manage&s=favorite',
      'sort' => 7,
    ),
    'user:manage:follow' => 
    array (
      0 => '/user/manage/follow',
      1 => 'api.php?app=user&do=manage&s=follow',
      'sort' => 9,
    ),
    'user:manage:phone' => 
    array (
      0 => '/user/manage/phone',
      1 => 'api.php?app=user&do=manage&s=phone',
      'sort' => 36,
    ),
    'user:manage:profile' => 
    array (
      0 => '/user/manage/profile',
      1 => 'api.php?app=user&do=manage&s=profile',
      'sort' => 35,
    ),
    'user:manage:vip' => 
    array (
      0 => '/user/manage/vip',
      1 => 'api.php?app=user&do=manage&s=vip',
      'sort' => 38,
    ),
    'user:message:inbox' => 
    array (
      0 => '/user/message/inbox',
      1 => 'api.php?app=user&do=message&s=inbox',
      'sort' => 34,
    ),
    'user:profile' => 
    array (
      0 => '/user/profile',
      1 => 'api.php?app=user&do=profile',
      'sort' => 10,
    ),
    'user:profile:avatar' => 
    array (
      0 => '/user/profile/avatar',
      1 => 'api.php?app=user&do=profile&pg=avatar',
      'sort' => 12,
    ),
    'user:profile:base' => 
    array (
      0 => '/user/profile/base',
      1 => 'api.php?app=user&do=profile&pg=base',
      'sort' => 11,
    ),
    'user:profile:bind' => 
    array (
      0 => '/user/profile/bind',
      1 => 'api.php?app=user&do=profile&pg=bind',
      'sort' => 14,
    ),
    'user:profile:custom' => 
    array (
      0 => '/user/profile/custom',
      1 => 'api.php?app=user&do=profile&pg=custom',
      'sort' => 15,
    ),
    'user:profile:setpassword' => 
    array (
      0 => '/user/profile/setpassword',
      1 => 'api.php?app=user&do=profile&pg=setpassword',
      'sort' => 13,
    ),
    'user:publish' => 
    array (
      0 => '/user/publish',
      1 => 'api.php?app=user&do=manage&pg=publish',
      'sort' => 2,
    ),
    'user:register' => 
    array (
      0 => '/user/register',
      1 => 'api.php?app=user&do=register',
      'sort' => 16,
    ),
    'user:reminder' => 
    array (
      0 => '/user/reminder',
      1 => 'api.php?app=user&do=reminder',
      'sort' => 32,
    ),
  ),
  'meta' => NULL,
);