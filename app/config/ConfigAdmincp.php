<?php

/**
 * iCMS - i Content Management System
 * Copyright (c) iCMSdev.com. All rights reserved.
 *
 * @author icmsdev <master@icmsdev.com>
 * @site https://www.icmsdev.com
 * @licence https://www.icmsdev.com/LICENSE.html
 */
class ConfigAdmincp extends AdmincpBase
{

    public function __construct()
    {
        parent::__construct();
    }
    /**
     * [配置管理]
     */
    public function do_manage()
    {
        $config    = Config::data();
        $redis     = extension_loaded('redis');
        $memcache  = extension_loaded('memcached');
        $tabsArray = $this->getMenuTabs();
        $extends   = Config::scan();
        Menu::setData('nav.active', $_SERVER['REQUEST_URI']);
        AppsMeta::get(self::$appId, Config::$siteid);
        include self::view("config.index");
    }
    /**
     * [保存配置]
     */
    public function ACTION_save()
    {
        $config = (array)Request::post('config');

        if ($msg = FilesClient::checkConf($config['FS'])) {
            self::alert($msg);
        }

        FilesClient::allowExt(trim($config['route']['ext'], '.')) or self::alert('URL设置 > 文件后缀设置不合法');

        $desktop_tpl_ext = File::getExt($config['template']['desktop']['tpl']);
        if ($desktop_tpl_ext) FilesClient::allowExt($desktop_tpl_ext) or self::alert("桌面端模板不合法");

        $config['route']['ext']    = '.' . trim($config['route']['ext'], '.');
        $config['route']['url']    = trim($config['route']['url'], '/');
        $config['route']['public'] = rtrim($config['route']['public'], '/');
        $config['route']['user']   = rtrim($config['route']['user'], '/');
        $config['route']['dir']    = rtrim($config['route']['dir'], '/') . '/';
        $config['FS']['url']        = trim($config['FS']['url'], '/') . '/';
        $config['template']['desktop']['domain'] = $config['route']['url'];

        if ($config['cache']['engine'] != 'file') {
            iPHP::callback(
                ["CacheHelper", "test"],
                [$config['cache']]
            );
        }

        foreach ($config as $n => $v) {
            Config::set($v, $n, 0);
        }
        AppsMeta::save(self::$appId, Config::$siteid);
        Config::cache();
        // self::success('保存成功');
    }
    /**
     * [更新系统设置缓存]
     *
     * @return void
     */
    public function ACTION_cache()
    {
        $this->autoCache();
    }
    /**
     * [autoCache 在更新所有缓存时，将会自动执行]
     */
    public static function autoCache()
    {
        Config::cache();
    }
    public function getMenuTabs()
    {
        $tabsArray = array();
        $children = Menu::$DATA['system']['children']['config']['children'];
        if ($children) foreach ($children as $index => $value) {
            parse_str($value['href'], $output);
            $active = $_GET['tab'] ? ($_GET['tab'] == $output['tab'] ? 'active' : '') : ($index ?: 'active');
            $id  = str_replace('.', '-', $output['tab']);
            $dir = $output['dir'];
            if (empty($dir)) {
                $dir = 'config';
                $output['tab'] = str_replace('.', '/', $output['tab']);
            }
            try {
                if (File::check($output['tab'])) {
                    $dir = Security::safeStr($dir);
                    $tabsArray[$index] = array($output['tab'], $value['caption'], $dir, $id, $active);
                }
            } catch (\Exception $ex) {
                $tabsArray[$index] = array('config/error', $value['caption'], 'config', $id, $active, $ex, $value);
            }
        }
        return $tabsArray;
    }
}
