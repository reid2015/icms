<?php

/**
 * iCMS - i Content Management System
 * Copyright (c) 2007-2017 iCMSdev.com. All rights reserved.
 *
 * @author icmsdev <master@icmsdev.com>
 * @site https://www.icmsdev.com
 * @licence https://www.icmsdev.com/LICENSE.html
 */
class Editor
{
    public static function ueditor($id, $config = array(), $gateway = true)
    {
        $gateway = $gateway ? 'admincp' : 'usercp';
        $app = $config['app']['app'];
        empty($app) && $app = 'content';
        ob_start();
        include AdmincpView::view("ueditor.script", "editor");
        return AdmincpView::html();
    }
    public static function markdown($id, $config = array(), $gateway = true)
    {
        $gateway = $gateway ? 'admincp' : 'usercp';
        $app = $config['app']['app'];
        empty($app) && $app = 'content';
        ob_start();
        include AdmincpView::view("markdown.script", "editor");
        return AdmincpView::html();
    }
}
